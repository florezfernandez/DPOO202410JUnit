package pruebas;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;

import java.time.Duration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import logica.Calculadora;

public class CalculadoraTest {
	private Calculadora calculadora;
	private long p = 80285903;
	
	@BeforeEach
	public void setUp() {
		this.calculadora = new Calculadora();
	}
	
	@Test
	public void divisionTest() throws Exception {
		assertEquals(3, this.calculadora.division(60, 20));
		Exception e = assertThrows(Exception.class, () -> this.calculadora.division(10, 0));
		assertEquals("No se puede", e.getMessage());
	}
	
	@Test
	public void esPrimoTest() {
		assertTrue(this.calculadora.esPrimo1(43));
		assertTrue(this.calculadora.esPrimo2(43));
		assertTrue(this.calculadora.esPrimo3(43));
		assertTrue(this.calculadora.esPrimo4(43));
		assertFalse(this.calculadora.esPrimo1(121));
		assertFalse(this.calculadora.esPrimo2(121));
		assertFalse(this.calculadora.esPrimo3(121));
		assertFalse(this.calculadora.esPrimo4(121));
		assertTrue(this.calculadora.esPrimo4(2));
		assertFalse(this.calculadora.esPrimo4(100));
	}
	
	@Test
	public void esPrimo4Test() {
		assertTimeout(Duration.ofMillis(1000), () -> {
			this.calculadora.esPrimo4(p);
	    });
	}
	
	@Test
	public void esPrimo3Test() {
		assertTimeout(Duration.ofMillis(1000), () -> {
			this.calculadora.esPrimo3(p);
	    });
	}
	
	
	@Test
	public void esPrimo2Test() {
		assertTimeout(Duration.ofMillis(1000), () -> {
			this.calculadora.esPrimo2(p);
	    });
	}
	
	@Test
	public void esPrimo1Test() {
		assertTimeout(Duration.ofMillis(1000), () -> {
			this.calculadora.esPrimo1(p);
	    });
	}
	
	@Test
	public void mcdTest() {
		assertEquals(50, this.calculadora.mcd(100, 50));
		assertEquals(91, this.calculadora.mcd(2366, 273));
	}
	
	
	
}
