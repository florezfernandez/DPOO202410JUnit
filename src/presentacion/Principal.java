package presentacion;

import logica.Calculadora;

public class Principal {

	public static void main(String[] args) {
		long p = 6443922871L;
		Calculadora calculadora = new Calculadora();
		long ti, tf; 
		ti = System.currentTimeMillis();
		calculadora.esPrimo4(p);
		tf = System.currentTimeMillis();
		System.out.println(tf-ti);
		
		ti = System.currentTimeMillis();
		calculadora.esPrimo3(p);
		tf = System.currentTimeMillis();
		System.out.println(tf-ti);

		ti = System.currentTimeMillis();
		calculadora.esPrimo2(p);
		tf = System.currentTimeMillis();
		System.out.println(tf-ti);
		
		ti = System.currentTimeMillis();
		calculadora.esPrimo1(p);
		tf = System.currentTimeMillis();
		System.out.println(tf-ti);

		
		
	}
}
