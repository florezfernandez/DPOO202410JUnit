package logica;

public class Calculadora {

	public double division(double a, double b) throws Exception {
		if(b != 0) {
			return a/b;
		}else {
			throw new Exception("No se puede");
		}
	}
	
	public boolean esPrimo1(long n) {
		for(long i=2; i<n; i++) {
			if(n%i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public boolean esPrimo2(long n) {
		for(long i=2; i<=n/2; i++) {
			if(n%i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public boolean esPrimo3(long n) {
		for(long i=2; i<=Math.sqrt(n); i++) {
			if(n%i == 0) {
				return false;
			}
		}
		return true;
	}

	public boolean esPrimo4(long n) {
		if(n == 2) {
			return true;
		}else if(n%2 == 0) {
			return false;
		}else {
			for(long i=3; i<=Math.sqrt(n); i+=2) {
				if(n%i == 0) {
					return false;
				}
			}			
		}
		return true;
	}
	
	public int mcd(int mayor, int menor) {
		if(mayor%menor == 0) {
			return menor;
		}else {
			return mcd(menor, mayor%menor);
		}
		
	}
}
